<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ua.kpi.model.Course"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="q" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="t" uri="/WEB-INF/tlds/taglib.tld" %>
<!DOCTYPE html>

<html>   
    <link rel="stylesheet" type="text/css" href ="jsp/css/catalog.css">
    <link rel="stylesheet" type="text/css" href ="jsp/css/log.css">
    <head><title>Your Courses</title></head>
    <body>
        <fieldset>
            <legend><h2><c:out value="${parametersMap.get('GoingCourses')}:"/></h2></legend>
            <table>
                <tr>  
                    <td class="search">
                          <br>                      
                        <br>
                        <form name="aForm" method="POST" action="controller">
                            <input type="hidden" name="command" value="teachp">
                            <input type="submit" value="${parametersMap.get('FinishedCourses')}" class="subo">
                        </form>
                        <form name="logoutForm" method="POST" action="controller">
                            <input type="hidden" name="command" value="logout">
                            <input type="submit" value="${parametersMap.get('Logout')}" class="subo">
                        </form>
                    </td>
                    <td>
                        <fieldset>
                            <form name="loginForm" method="POST" action="controller">
                                <table class="inner_table" align="center">
                                    <tr>
                                        <th><c:out value="${parametersMap.get('Course')}:"/></th>
                                        <th><c:out value="${parametersMap.get('Surname')}:"/></th>         
                                    </tr>                                                                     
                                    <c:forEach items="${datalist}" var="list">            
                                        <tr>
                                            <td align="center">
                                                <c:out value="${list.course.name}"/> 
                                            </td>
                                            <td align="center">
                                                <c:out value="${list.student.surname}"/> 
                                            </td>
                                            <form name="lolForm" method="POST" action="controller">
                                            <td align="center">                                                 
                                                  <select name="mark">
                                                    <option></option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                  </select>                                                 
                                            </td>
                                            <td align="center">                                           
                                                 <input type="hidden" name="command" value="assess${list.id}">                
                                                 <input type="submit" value="${parametersMap.get('Change')}" class="subo">                                                
                                            </td>
                                            </form>
                                        </tr>	
                                    </c:forEach>
                                    <tr>
                                        <td></td>
                                        <td></td>                                       
                                    </tr>
                                </table>
                            </form>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </fieldset>
    </body>

</html>
