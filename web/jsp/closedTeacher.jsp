<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ua.kpi.model.Course"%>
<%@page import="ua.kpi.model.User"%>
<%@page import="ua.kpi.model.Archive"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="q" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="t" uri="/WEB-INF/tlds/taglib.tld" %>
<!DOCTYPE html>

<html>   
    <link rel="stylesheet" type="text/css" href ="jsp/css/catalog.css">
    <link rel="stylesheet" type="text/css" href ="jsp/css/log.css">
    <head><title>Closed teacher page</title></head>
    <body>
        <fieldset>
            <legend><h2><c:out value="${parametersMap.get('FinishedCourses')}:"/></h2></legend>
            <table>
                <tr>  
                    <td class="search">
                          <br>                      
                        <br>
                        <form name="aForm" method="POST" action="controller">
                            <input type="hidden" name="command" value="tlookp">
                            <input type="submit" value="${parametersMap.get('GoingCourses')}" class="subo">
                        </form>
                        <form name="logoutForm" method="POST" action="controller">
                            <input type="hidden" name="command" value="logout">
                            <input type="submit" value="${parametersMap.get('Logout')}" class="subo">
                        </form>
                    </td>
                    <td>
                        <fieldset>
                            <form name="loginForm" method="POST" action="controller">
                                <table class="inner_table" align="center">
                                    <tr>
                                        <th><c:out value="${parametersMap.get('Course')}:"/></th>
                                        <th><c:out value="${parametersMap.get('Surname')}:"/></th>
                                        <th><c:out value="${parametersMap.get('Mark')}:"/></th>
                                    </tr>                                                                     
                                    <c:forEach items="${datalist}" var="list">            
                                        <tr>
                                            <td align="center">
                                                <c:out value="${list.course.name}"/> 
                                            </td>
                                            <td align="center">
                                                <c:out value="${list.student.surname}"/> 
                                            </td>                                           
                                            <td align="center">
                                                <c:out value="${list.mark}"/> 
                                            </td>    
                                        </tr>	
                                    </c:forEach>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </form>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </fieldset>
    </body>

</html>
