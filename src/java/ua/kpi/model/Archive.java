/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.model;

import java.util.Objects;

/**
 *
 * @author lor1an
 */
public class Archive {


    private int id;
    private Course course;
    private User student;
    private int mark;


    public Archive() {
    }

    public Archive(int id, Course course, User student, int mark) {
        this.id = id;
        this.course = course;
        this.student = student;
        this.mark = mark;
    }

    public Archive(Course course, User student) {
        this.course = course;
        this.student = student;

    }
    
    public void setId(int id) {
        this.id = id;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
    
    public void setCourse(Course course) {
        this.course = course;
    }

    public void setStudent(User student) {
        this.student = student;
    }

    public User getStudent() {
        return student;
    }

    public int getId() {
        return id;
    }

    public Course getCourse() {
        return course;
    }

    public int getMark() {
        return mark;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Archive other = (Archive) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.student, other.student)) {
            return false;
        }
        if (!Objects.equals(this.course, other.course)) {
            return false;
        }
        if (!Objects.equals(this.mark, other.mark)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "Archive{" + "id=" + id + ", studentID=" + student + ", courseName=" + course.getName() + ", mark=" + mark + '}';
    }

   
}
