package ua.kpi.model;

import java.util.Objects;

/**
 *
 * @author lor1an
 */
public class Course {

    private int id;
    private String name;
    private User teacher;

    public Course(int id, String name, User teacher) {
        this.id = id;
        this.name = name;
        this.teacher = teacher;
    }

    public Course(String name,  User teacher) {
        this.name = name;
        this.teacher = teacher;
    }

    public Course(String name) {
        this.name = name;

    }

    public Course() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public User getTeacher() {
        return teacher;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        hash = 97 * hash + this.teacher.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Course other = (Course) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }

        if (this.teacher != other.teacher) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Course{" + "id=" + id + ", name=" + name + ", teacher name=" + teacher.getName() + ", teacher surname=" + teacher.getSurname()+'}';
    }
}
