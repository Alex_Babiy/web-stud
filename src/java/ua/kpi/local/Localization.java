/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.local;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author lor1an
 */
public class Localization {

    private static Localization instance;
    //класс извлекает информацию из файла messages.properties
    public static final String LIB_BUNDLENAME = "ua.kpi.local.lib";
    //название локализованых переменных
    public static final String[] LIB_NAMES = {"PleaseLogin", "Username", "Password",
        "Login", "Register", "Lang", "Local", "CatalogTitle", "Search", "Requests",
        "EditLibrary", "MyRequests", "Logout", "Course", "Mark","Forward","MyCourses","OpenCourses",
        "Stock", "Order", "Submit", "Student", "Teacher", "Catalog", "ErrorLogMessage", "SucRegMessage",
        "MainPage", "AddBook", "Add","MarkChanged", "Change", "RequestsTitle", "UserRequestsTitle",
        "Status", "Response", "Reply", "Subscription", "Approve", "AName","StudentCoursesList","GoingCourses",
        "ASurname", "TitleSearchM", "AuthorSearchM", "GenreSearchM", "LogMessage1","CourseGoingOn","FinishedCourses",
        "LogMessage2", "Name", "Surname", "PleaseRegister", "ConfirmPageTitle", "ConfirmReturn", "Confirm"
    };

    public static ResourceBundle doLocale(String language, String country,
            String bundleName) {
        return ResourceBundle.getBundle(bundleName, new Locale(language, country));
    }

    public static Localization getInstance() {
        if (instance == null) {
            instance = new Localization();
        }
        return instance;
    }
}
