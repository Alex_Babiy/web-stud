/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.servlet.commands;

import ua.kpi.DAO.CourseDAO;
import ua.kpi.DAO.UserDAO;
import ua.kpi.DAO.ArchiveDAO;
import ua.kpi.DAO.DAOFactory;
import ua.kpi.exceptions.DAOException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ua.kpi.logic.PageContentMaker;
import ua.kpi.manager.PathManager;
import ua.kpi.model.Course;
import ua.kpi.model.User;
import ua.kpi.model.Archive;
import ua.kpi.exceptions.DAOException;
import static ua.kpi.servlet.commands.Command.logger;

/**
 *
 * @author lor1an
 */
public class AssessmentCommand implements Command {

    
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Start Order command;");         
        PageContentMaker.makeAssess(request);
        PageContentMaker.makeTeacherClosedContent(request);
        HttpSession session = request.getSession(false);
        session.setAttribute("CurrentPagePath", PathManager.TEACHER_CLOSED_COURSES);
        return PathManager.getInstance().getProperty(PathManager.TEACHER_CLOSED_COURSES);
    }
}
