/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.servlet;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import ua.kpi.servlet.commands.Command;
import ua.kpi.servlet.commands.GoToSearchPageCommand;
import ua.kpi.servlet.commands.GoToClosedTeacherCommand;
import ua.kpi.servlet.commands.GoToStudentPageCommand;
import ua.kpi.servlet.commands.GoToOpenedTeacherCommand;
import ua.kpi.servlet.commands.GoToOpenCoursePageCommand;
import ua.kpi.servlet.commands.LocaleCommand;
import ua.kpi.servlet.commands.LoginCommand;
import ua.kpi.servlet.commands.LogoutCommand;
import ua.kpi.servlet.commands.NoCommand;
import ua.kpi.servlet.commands.OrderCommand;
import ua.kpi.servlet.commands.GoToRegisterPageCommand;
import ua.kpi.servlet.commands.SubmitCommand;
import ua.kpi.servlet.commands.AssessmentCommand;

/**
 *
 * @author lor1an
 */
public class RequestHelper {

    private static RequestHelper instance = null;
    HashMap<String, Command> commands = new HashMap<>();

    private RequestHelper() {
        //заполнение таблицы командами
        commands.put("login", new LoginCommand());
        commands.put("register", new GoToRegisterPageCommand());
        commands.put("submit", new SubmitCommand());
        commands.put("order", new OrderCommand());
        commands.put("exsearch", new GoToSearchPageCommand());
        commands.put("tlookp", new GoToOpenedTeacherCommand());
        commands.put("openc", new GoToOpenCoursePageCommand());
        commands.put("readp", new GoToStudentPageCommand());
        commands.put("teachp", new GoToClosedTeacherCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("EN", new LocaleCommand());
        commands.put("RU", commands.get("EN"));
        //commands.put("assess", new AssessmentCommand());
    }

    public Command getCommand(HttpServletRequest request) {
        //извлечение команды из запроса
        String action = request.getParameter("command");
        if (action.startsWith("assess")) {
            return new AssessmentCommand();
        }

        //получение объекта, соответствующего команде
        Command command = commands.get(action);
        if (command == null) {
            //если команды не существует в текущем объекте
            command = new NoCommand();
        }
        return command;
    }
    //создание единственного объекта по шаблону Singleton

    public static RequestHelper getInstance() {
        if (instance == null) {
            instance = new RequestHelper();
        }
        return instance;
    }
}
