/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.DAO.Interfaces;

import java.util.List;
import ua.kpi.exceptions.DAOException;
import ua.kpi.model.Archive;
import ua.kpi.model.User;

/**
 *
 * @author lor1an
 */
public interface ArchiveCRUD extends CRUD <Archive> {
        
      //public List<Archive> getAll() throws DAOException;
      public List<Archive> findOpenForStudent(User student) throws DAOException;
      
      public List<Archive> findByStudent(User student) throws DAOException;
}
