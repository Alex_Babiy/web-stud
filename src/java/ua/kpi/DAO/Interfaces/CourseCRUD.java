/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.DAO.Interfaces;

import ua.kpi.exceptions.DAOException;
import java.util.List;
import ua.kpi.model.Course;


/**
 *
 * @author lor1an
 */
public interface CourseCRUD extends CRUD<Course> {

    public List<Course> getAll() throws DAOException;

    public Course findByName(String name) throws DAOException;


}
