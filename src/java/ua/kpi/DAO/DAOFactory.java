/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.DAO;

import org.apache.log4j.Logger;

/**
 *
 * @author lor1an
 */
public class DAOFactory {

    static Logger logger = Logger.getLogger(DAOFactory.class);

    public DAOFactory(Logger log) {
        logger = log;
    }

    public CourseDAO getCourseDAO() {
        return new CourseDAO(logger);
    }

    public UserDAO getUsetDAO() {
        return new UserDAO(logger);
    }

    public ArchiveDAO getArchiveDAO() {
        return new ArchiveDAO(logger);
    }

}