/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.DAO;

import ua.kpi.DAO.Interfaces.ArchiveCRUD;
import ua.kpi.exceptions.DAOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ua.kpi.model.Archive;
import ua.kpi.model.User;
import ua.kpi.model.Course;
import org.apache.log4j.Logger;

/**
 *
 * @author lor1an
 */
public class ArchiveDAO extends BasicCRUD implements ArchiveCRUD {

    private static final String ADD_ARCHIVE = "insert into archive(courseID, studentID) values (?,?);";
    private static final String UPDATE_ARCHIVE_MARK = "update archive set mark=? where ID =?;";
    private static final String FIND_ARCHIVES_BY_STUDENT = "select us.surname,e.name,mark from ( select c.name,courseID,teacherID,mark from archive a inner join course c on a.courseID=c.ID inner join users u on a.studentID=u.ID where u.ID=?) e inner join users us on e.teacherID=us.ID;";
    private static final String FIND_ARCHIVES_WITHOUT_STUDENT = "select c.name,t.surname from course c\n" +
                                                                "inner join users t on c.teacherID=t.ID\n" +
                                                                "where c.ID NOT IN\n" +
                                                                "(select c.ID from course c\n" +
                                                                "left join archive a on c.ID=a.courseID\n" +
                                                                "where a.studentID=?);";
    private static final String FIND_ARCHIVE_BY_ID = "select name,courseID, studentID, mark from archive where ID=?;";
    private static final String FIND_OPENED_BY_TEACHER = "select us.surname,e.name,e.id from (\n" +
                                                         "	select c.name,courseID,studentID,a.mark,a.id from course c \n" +
                                                         "	inner join archive a on a.courseID=c.ID \n" +
                                                         "	left join users u on c.teacherID=u.ID \n" +
                                                         "	where u.ID=? and a.mark is null) e\n" +
                                                         "inner join users us on e.studentID=us.ID;";
    private static final String FIND_CLOSED_BY_TEACHER = "select us.surname,e.name,e.mark from (select c.name,courseID,studentID,a.mark from course c inner join archive a on a.courseID=c.ID inner join users u on c.teacherID=u.ID where u.ID=? and a.mark is not null) e inner join users us on e.studentID=us.ID;";   
    private static final String DELETE_ARCHIVE = "delete from archive where id=?";

    ArchiveDAO(Logger logger) {
        super(logger);
    }

    @Override
    public Archive findById(int id) throws DAOException {
        Archive archive_note = new Archive();
        try {
            super.makeConnection();
            Object args[] = {id};
            result = super.getItems(FIND_ARCHIVE_BY_ID, args);
            if (result.next()) {
                archive_note.setId(id);
                User student = new User();
                student.setId(result.getInt("studentID"));
                archive_note.setStudent(student);
                Course course = new Course();
                course.setId(result.getInt("courseID"));
                archive_note.setCourse(course);
                archive_note.setMark(result.getInt("mark"));              
            }
            super.closeConnection();
        } catch (SQLException ex) {
            logger.info("Sql exception thrown;");
            logger.error("Exception thrown!", ex);
        }
        return archive_note;
    }

    @Override
    public boolean update(Archive entity) throws DAOException {
        int resultValue;
        super.makeConnection();
        Object args[] = {entity.getMark() , entity.getId()};
        resultValue = super.executeQuery(UPDATE_ARCHIVE_MARK, args);
        super.closeConnection();
        if (resultValue > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int save(Archive entity) throws DAOException {
        int resultValue;
        super.makeConnection();
        Object args[] = {entity.getCourse().getId(), entity.getStudent().getId()};
        resultValue = super.executeQuery(ADD_ARCHIVE, args);
        super.closeConnection();
        if (resultValue > 0) {
            return resultValue;
        } else {
            return 0;
        }
    }

    @Override
    public boolean delete(Archive entity) throws DAOException {
        int resultValue;
        super.makeConnection();
        Object args[] = {entity.getId()};
        resultValue = super.executeQuery(DELETE_ARCHIVE, args);
        if (resultValue > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Archive> findOpenForStudent(User student) throws DAOException {
        List<Archive> archive_notesList = new ArrayList<>();
        try {
            super.makeConnection();
            Object[] args = {student.getId()};
            result = super.getItems(FIND_ARCHIVES_WITHOUT_STUDENT, args);
            if (result == null) {
                archive_notesList.add(new Archive());
                return archive_notesList;
            }
            while (result.next()) {
                Archive archive_note = new Archive();
                User teacher = new User();
                teacher.setSurname(result.getString("surname"));
                Course course = new Course();
                course.setName(result.getString("name"));
                course.setTeacher(teacher);
                archive_note.setCourse(course);
                archive_notesList.add(archive_note);
            }
            super.closeConnection();
        } catch (SQLException ex) {
            logger.info("Sql exception thrown;");
            logger.error("Exception thrown!", ex);
        }
        return archive_notesList;

    }
    
    @Override
    public List<Archive> findByStudent(User student) throws DAOException {
        List<Archive> archive_notesList = new ArrayList<>();
        try {
            super.makeConnection();
            Object[] args = {student.getId()};            
            result = super.getItems(FIND_ARCHIVES_BY_STUDENT, args);
            while (result.next()) {
                Archive archive_note = new Archive();                                          
                User teacher = new User();
                teacher.setSurname(result.getString("surname"));
                Course course = new Course();
                course.setName(result.getString("name"));
                course.setTeacher(teacher);
                archive_note.setMark(result.getInt("mark")); 
                archive_note.setCourse(course);
                archive_notesList.add(archive_note);
            }
            super.closeConnection();
        } catch (SQLException ex) {
            logger.info("Sql exception thrown;");
            logger.error("Exception thrown!", ex);
        }
        return archive_notesList;

    }

    public List<Archive> findClosedByTeacher(User teacher) throws DAOException {
        List<Archive> archive_notesList = new ArrayList<>();
        try {
            super.makeConnection();
            Object[] args = {teacher.getId()};
            result = super.getItems(FIND_CLOSED_BY_TEACHER, args);
            while (result.next()) {
                Archive archive_note = new Archive();  
                archive_note.setMark(result.getInt("mark"));
                Course course = new Course();
                course.setName(result.getString("name"));
                archive_note.setCourse(course);
                User student = new User();
                student.setSurname(result.getString("surname"));
                archive_note.setStudent(student);                        
                archive_notesList.add(archive_note);
            }
            super.closeConnection();
        } catch (SQLException ex) {
            logger.info("Sql exception thrown;");
            logger.error("Exception thrown!", ex);
        }
        return archive_notesList;

    }
    
    public List<Archive> findOpenedByTeacher(User teacher) throws DAOException {
        List<Archive> archive_notesList = new ArrayList<>();
        try {
            super.makeConnection();
            Object[] args = {teacher.getId()};
            result = super.getItems(FIND_OPENED_BY_TEACHER, args);
            while (result.next()) {
                Archive archive_note = new Archive();
                Course course = new Course();
                course.setName(result.getString("name"));
                archive_note.setCourse(course);
                User student = new User();
                student.setSurname(result.getString("surname"));
                archive_note.setStudent(student);
                archive_note.setId(result.getInt("id"));
                archive_notesList.add(archive_note);
            }
            super.closeConnection();
        } catch (SQLException ex) {
            logger.info("Sql exception thrown;");
            logger.error("Exception thrown!", ex);
        }
        return archive_notesList;

    }
}