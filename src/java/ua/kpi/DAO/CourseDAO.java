/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.DAO;

import ua.kpi.DAO.Interfaces.CourseCRUD;

import ua.kpi.exceptions.DAOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ua.kpi.model.Course;
import ua.kpi.model.User;
import org.apache.log4j.Logger;

/**
 *
 * @author lor1an
 */
public class CourseDAO extends BasicCRUD implements CourseCRUD {

    private static final String ADD_COURSE = "insert into course(Name, teacherID) values (?,?);";
    private static final String GET_ALL_COURSES = "select ID, Name, teacherID from course;";
    private static final String FIND_COURSE_BY_ID = "select Name, teacherID from course where ID=?";
    private static final String FIND_COURSES_BY_NAME = "select ID, teacherID from course where Name=?;";
    private static final String UPDATE_COURSE = "update course set Name=?, teacherID=? where ID=?;";
    private static final String DELETE_COURSE = "delete from course where ID=?";

    CourseDAO(Logger logger) {
        super(logger);
    }

    @Override
    public Course findById(int id) throws DAOException {
        Course course = null;
        try {
            super.makeConnection();
            Object[] args = {id};
            result = super.getItems(FIND_COURSE_BY_ID, args);
            UserDAO userdao = new UserDAO(super.logger);

            if (result.next()) {
                course = new Course();
                course.setId(id);
                course.setName(result.getString("Name"));
                User teacher = userdao.findById(result.getInt("teacherID"));
                course.setTeacher(teacher);
            }
            super.closeConnection();
        } catch (SQLException ex) {
            logger.info("Sql exception thrown");
            logger.error("Exception thrown!", ex);
        }
        return course;
    }

    @Override
    public boolean update(Course entity) throws DAOException {
        int resultValue;
        super.makeConnection();
        Object args[] = {entity.getName(), entity.getTeacher().getId(), entity.getId()};
        resultValue = super.executeQuery(UPDATE_COURSE, args);
        super.closeConnection();
        if (resultValue > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int save(Course entity) throws DAOException {
        int resultValue;
        super.makeConnection();
        Object args[] = {entity.getName(), entity.getTeacher().getId()};
        resultValue = super.executeQuery(ADD_COURSE, args);
        super.closeConnection();
        if (resultValue > 0) {
            return resultValue;
        } else {
            return 0;
        }
    }

    @Override
    public boolean delete(Course entity) throws DAOException {
        int resultValue;
        super.makeConnection();
        Object args[] = {entity.getId()};
        resultValue = super.executeQuery(DELETE_COURSE, args);
        if (resultValue > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Course> getAll() throws DAOException {
        List<Course> courses = new ArrayList<>();
        try {
            super.makeConnection();
            Object args[] = {};
            result = super.getItems(GET_ALL_COURSES, args);
            UserDAO userdao = new UserDAO(super.logger);
            while (result.next()) {
                Course course = new Course();
                course.setId(result.getInt("Id"));
                User teacher = userdao.findById(result.getInt("teacherID"));
                course.setTeacher(teacher);
                course.setName(result.getString("Name"));               
                courses.add(course);
            }
            super.closeConnection();
        } catch (SQLException ex) {
            logger.info("Sql exception thrown");
            logger.error("Exception thrown!", ex);
        }
        return courses;
    }

    @Override
    public Course findByName(String name) throws DAOException {
        Course course = null;
        try {
            super.makeConnection();
            Object[] args = {name};
            result = super.getItems(FIND_COURSES_BY_NAME, args);
            if (result.next()) {
                course = new Course();
                UserDAO userdao = new UserDAO(super.logger);
                course.setId(result.getInt("ID"));
                User teacher = userdao.findById(result.getInt("teacherID"));
                course.setTeacher(teacher);
                course.setName(name);
                
            }
            super.closeConnection();
        } catch (SQLException ex) {
            logger.info("Sql exception thrown;");
            logger.error("Exception thrown!", ex);
        }
        return course;
    }

}
