/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.logic;

import java.util.Enumeration;
import ua.kpi.DAO.CourseDAO;
import ua.kpi.DAO.UserDAO;
import ua.kpi.DAO.ArchiveDAO;
import ua.kpi.DAO.DAOFactory;
import ua.kpi.exceptions.DAOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import ua.kpi.model.Course;
import ua.kpi.model.Archive;
import ua.kpi.model.User;
import static ua.kpi.servlet.commands.Command.logger;

/**
 *
 * @author lor1an
 */
public class PageContentMaker {

    public static void makeTeacherClosedContent(HttpServletRequest request) {
        DAOFactory factory = new DAOFactory(logger);
        HttpSession session = request.getSession(false);
        ArchiveDAO archivedao = factory.getArchiveDAO();
        UserDAO userdao = factory.getUsetDAO();
        try {
            List<Archive> list = archivedao.findClosedByTeacher(userdao.findByLogin((String) session.getAttribute("name")));
            request.setAttribute("datalist", list);
        } catch (DAOException ex) {
            logger.info("DAOException was thrown");
            logger.error("Exception was thrown:", ex);
        }
    }

    public static void makeTeacherOpenedContent(HttpServletRequest request) {
        DAOFactory factory = new DAOFactory(logger);
        HttpSession session = request.getSession(false);
        ArchiveDAO archivedao = factory.getArchiveDAO();
        UserDAO userdao = factory.getUsetDAO();
        try {
            List<Archive> list = archivedao.findOpenedByTeacher(userdao.findByLogin((String) session.getAttribute("name")));
            request.setAttribute("datalist", list);
        } catch (DAOException ex) {
            logger.info("DAOException was thrown");
            logger.error("Exception was thrown:", ex);
        }
    }

    public static void makeStudentPageContent(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        DAOFactory factory = new DAOFactory(logger);
        ArchiveDAO archivedao = factory.getArchiveDAO();
        UserDAO userdao = factory.getUsetDAO();
        try {
            List<Archive> list = archivedao.findByStudent(userdao.findByLogin((String) session.getAttribute("name")));
            request.setAttribute("datalist", list);
        } catch (DAOException ex) {
            logger.info("DAOException was thrown");
            logger.error("Exception was thrown:", ex);
        }
    }

    public static void makeOpenCourseContent(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        DAOFactory factory = new DAOFactory(logger);
        ArchiveDAO archivedao = factory.getArchiveDAO();
        UserDAO userdao = factory.getUsetDAO();
        try {
            List<Archive> list = archivedao.findOpenForStudent(userdao.findByLogin((String) session.getAttribute("name")));
            request.setAttribute("datalist", list);
        } catch (DAOException ex) {
            logger.info("DAOException was thrown");
            logger.error("Exception was thrown:", ex);
        }
    }
    
    public static void makeOrder(HttpServletRequest request){
        String name;
        String[] result;
        HttpSession session = request.getSession(false);
        try {
            DAOFactory dfactory = new DAOFactory(logger);
            CourseDAO coursedao = dfactory.getCourseDAO();
            ArchiveDAO archivedao = dfactory.getArchiveDAO();
            UserDAO userdao = dfactory.getUsetDAO();
            Enumeration names = request.getParameterNames();
            while (names.hasMoreElements()) {
                name = (String) names.nextElement();
                result = name.split(";");
                if (result.length==2){
                    Course course = coursedao.findByName(result[0]);
                    User student = userdao.findByLogin((String) session.getAttribute("name"));
                    Archive archive = new Archive(course,student);
                    archivedao.save(archive);
                }
            }              
        } catch (DAOException ex) {
            logger.info("DAOException was thrown");
            logger.error("Exception was thrown:", ex);
        }
    }
    
    public static void makeAssess(HttpServletRequest request){
        String name;
        try {
            DAOFactory dfactory = new DAOFactory(logger);
            ArchiveDAO archivedao = dfactory.getArchiveDAO();        
            Enumeration names = request.getParameterNames();
          
                name = (String) names.nextElement(); 
                System.out.println("name: "+name);
                System.out.println("value: "+ request.getParameter(name));
                Archive archive = new Archive();
                System.out.println(new Integer(request.getParameter("command").substring(6)));
                archive.setId(new Integer(request.getParameter("command").substring(6)));
                archive.setMark(new Integer(request.getParameter("mark")));
                archivedao.update(archive);
                          
        } catch (DAOException ex) {
            logger.info("DAOException was thrown");
            logger.error("Exception was thrown:", ex);
        }
    }
}
