CREATE TABLE users (ID INT AUTO_INCREMENT, Name VARCHAR (35) NOT NULL, Surname VARCHAR(35) NOT NULL, Login VARCHAR(35) NOT NULL, Password VARCHAR(35) NOT NULL, IsTeacher BIT NOT NULL, CONSTRAINT usersPrimary PRIMARY KEY (ID));

CREATE TABLE archive (ID INT AUTO_INCREMENT, courseID INT NOT NULL, studentID INT NOT NULL, mark INT, CONSTRAINT archivePrime PRIMARY KEY (ID));

CREATE TABLE course (ID INT AUTO_INCREMENT, name VARCHAR(35) NOT NULL, teacherID INT NOT NULL, CONSTRAINT coursePrime PRIMARY KEY (ID));

---------------------------------------------------------------

ALTER TABLE course ADD CONSTRAINT usersForeign FOREIGN KEY (teacherID)  REFERENCES users(ID);

ALTER TABLE archive ADD CONSTRAINT archiveForeign FOREIGN KEY (studentID)  REFERENCES users(ID);

ALTER TABLE archive ADD CONSTRAINT courseForeign FOREIGN KEY (courseID)  REFERENCES course(ID);
---------------------------------------------------------------

INSERT INTO users (Name, Surname, Login, Password, isTeacher) values ('John', 'Nigga', 'lorian', 'lorian', 0);
INSERT INTO users (Name, Surname, Login, Password, isTeacher) values ('Borland', 'Pascal', 'borland', 'pass', 0);
INSERT INTO users (Name, Surname, Login, Password, isTeacher) values ('Udel', 'Plebeev', 'udel', 'pass', 0);
INSERT INTO users (Name, Surname, Login, Password, isTeacher) values ('Lena', 'Arena', 'lena', 'pass', 0);
INSERT INTO users (Name, Surname, Login, Password, isTeacher) values ('Walter', 'White', 'walt', 'pass', 1);
INSERT INTO users (Name, Surname, Login, Password, isTeacher) values ('John', 'Dorian', 'whitebear', 'pass', 1);
INSERT INTO users (Name, Surname, Login, Password, isTeacher) values ('Andrew', 'Dorogovtsev', 'pizdec', 'pass', 1);

---------------------------------------------------------------

INSERT INTO course (Name,teacherID) values ('Chemistry', '7'); 
INSERT INTO course (Name,teacherID) values ('Medicine', '8'); 
INSERT INTO course (Name,teacherID) values ('Theory of probability', '9'); 

-----------------------------------------------------------------

INSERT INTO archive (courseID,studentID,mark) values ('5', '1', '5'); 
INSERT INTO archive (courseID,studentID,mark) values ('5', '5', '2'); 
INSERT INTO archive (courseID,studentID,mark) values ('6', '4', '4'); 

 -----------------------------------------------------------------

INSERT INTO archive (courseID,studentID) values ('6', '1');
INSERT INTO archive (courseID,studentID) values ('4', '1');

 -----------------------------------------------------------------
select us.surname,e.name,mark from (
	select c.name,courseID,teacherID,mark from archive a 
	inner join course c on a.courseID=c.ID 
	inner join users u on a.studentID=u.ID 
	where u.ID='1') e
inner join users us on e.teacherID=us.ID;

 -----------------------------------------------------------------

select us.surname,e.name,e.mark from (
	select c.name,courseID,studentID,a.mark from course c 
	inner join archive a on a.courseID=c.ID 
	inner join users u on c.teacherID=u.ID 
	where u.ID='2' and a.mark is not null) e
inner join users us on e.studentID=us.ID;

 -----------------------------------------------------------------
select c.name,t.surname from course c
inner join users t on c.teacherID=t.ID
where c.ID NOT IN
(select c.ID from course c
left join archive a on c.ID=a.courseID
where a.studentID=1);

 -----------------------------------------------------------------
select e.name,e.surname,ar.mark from(
	select c.name,t.surname,c.ID from course c
	inner join users t on c.teacherID=t.ID
	where c.ID IN
		(select c.ID from course c
		left join archive a on c.ID=a.courseID
		where a.studentID=1)
	) e
inner join archive ar on e.ID=ar.courseID;



select us.surname,e.name,e.mark from (select c.name,courseID,studentID,a.mark from course c inner join archive a on a.courseID=c.ID inner join users u on c.teacherID=u.ID where u.ID='2' and a.mark is not null) e inner join users us on e.studentID=us.ID; 